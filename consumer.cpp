//
// Created by Haito on 10.01.2018.
//

#include <unistd.h>
#include <iostream>
#include "consumer.h"

void Consumer::consume(int descriptor, std::function<void(char *)> callback) {
//    int buffer;
//    while (read(descriptor, &buffer, sizeof(int))) {
//        if (buffer >= 0 && buffer < 255){
//            char new_buffer = (char) buffer;
//            callback(&new_buffer);
//        }
//    }
    while (true){
        char buffer;
        ssize_t retval = read(descriptor, &buffer, sizeof(buffer));

        //Koniec danych
        if (retval <= 0){
            if (retval == -1) //EBADF -> Ostatni odczyt. Koniec danych, po zamknięciu.
            {
                //perror("Read error handlable");
                //exit(6);
            }
            else {
                exit(0);
            }
        }
        else{
            callback(&buffer);
        }
    }
}
