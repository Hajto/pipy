//
// Created by Haito on 10.01.2018.
//

#ifndef PROJECTS_FILE_STREAM_H
#define PROJECTS_FILE_STREAM_H


#include <string>
#include <functional>

namespace FileStream {
    void streamFile(std::string name, std::function<void (char*)> onDataPacket);
    bool fileExists (const std::string& name);
    void ensureFifoPresent(std::string name, mode_t mode);
};


#endif //PROJECTS_FILE_STREAM_H
