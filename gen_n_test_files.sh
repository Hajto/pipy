#!/bin/bash

echo "Creating $1 files"
for ((i=1; i <= $1; i++)); do
    ./generate_test_source.sh $2 cmake-build-debug/producer_src/$i
done
