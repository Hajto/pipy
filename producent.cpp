//
// Created by Haito on 10.01.2018.
//

#include <unistd.h>
#include "producent.h"


void Producent::produce(int writeDescriptor, char data) {
    write(writeDescriptor, &data, sizeof(data));
}
