//
// Created by Haito on 10.01.2018.
//

#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include "producent.h"
#include "file_stream.h"
#include "consumer.h"
#include <fstream>
#include <unistd.h>
#include <iostream>
#include <fcntl.h>

int main(int arg_count, char **args) {
    if (arg_count != 3) throw std::runtime_error("Wrong number of arguemnts expected 2");

    std::string source(args[1]);
    std::string pipeName(args[2]);

    FileStream::ensureFifoPresent(pipeName, S_IWUSR | S_IRUSR);

    /*
     * Consumer for testing
     */
    FILE *file = fopen(source.c_str(), "w");
    int rpipe = open(pipeName.c_str(), O_RDONLY | O_NONBLOCK);
    if (file != NULL && rpipe != -1){
        Consumer::consume(rpipe, [=, &file](char *c) {
            fputc(*c, file);
            //std::cout<<c;
        });
        fclose(file);
    }

}