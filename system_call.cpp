//
// Created by Haito on 13.12.2017.
//

#include <string>
#include <array>
#include <memory>
#include "system_call.h"

#define BUFFER_SIZE 128

/*
 * std::array http://en.cppreference.com/w/cpp/container/array
 * Feof: http://www.cplusplus.com/reference/cstdio/feof/
 * Shared pointer: http://en.cppreference.com/w/cpp/memory/shared_ptr
 * Pipe: https://linux.die.net/man/2/pipe
 */

std::string SystemCall::call_command(const std::string &command) {
    std::array<char, BUFFER_SIZE> buffer{};
    std::string result;
    std::shared_ptr<FILE> pipe(popen(command.c_str(), "r"), pclose);

    if (!pipe)
        throw std::runtime_error("popen() failed!");

    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), BUFFER_SIZE, pipe.get()) != nullptr)
        result += buffer.data();
    }

    return result;
}
