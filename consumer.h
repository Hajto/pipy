//
// Created by Haito on 10.01.2018.
//

#ifndef PROJECTS_CONSUMER_H
#define PROJECTS_CONSUMER_H


#include <functional>

namespace Consumer {
    void consume(int descriptor, std::function<void (char*)>);
};


#endif //PROJECTS_CONSUMER_H
