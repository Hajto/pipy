//
// Created by Haito on 10.01.2018.
//

#include <stdexcept>
#include <string>
#include "file_stream.h"
#include "producent.h"
#include <fstream>
#include <iostream>

#include <unistd.h>
#include <fcntl.h>

int main(int arg_count, char **args) {
    if (arg_count != 3) throw std::runtime_error("Wrong number of arguemnts expected 2");

    std::string source(args[1]);
    std::string pipeName(args[2]);

    FileStream::ensureFifoPresent(pipeName, S_IWUSR | S_IRUSR | S_IWGRP | S_IRGRP);
    int pipe = open(pipeName.c_str(), O_WRONLY);
    if ( pipe != -1) {
        FileStream::streamFile(source, [pipe](char* data){
            //std::cout<<"Emiting:"<<*data<<std::endl;
            Producent::produce(pipe, *data);
        });
    } else {
        throw std::runtime_error("Failed to open fifo or input file");
    }
    close(pipe);

}