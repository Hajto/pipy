//
// Created by Haito on 10.01.2018.
//

#include "file_stream.h"
#include <fstream>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>

void FileStream::streamFile(std::string name, std::function<void(char *)> onDataPacket) {
    std::ifstream read;
    read.open(name.c_str());

    //Brzydkie ale chyba lepsze niż random return itd
    if (read.good()) {
        char c;
        while (read.get(c)){
            if (c != EOF && c != 0){
                onDataPacket(&c);
            }
        }
        read.close();
    } else {
        throw std::runtime_error("Could not open file");
    }
}

void FileStream::ensureFifoPresent(std::string name, mode_t mode) {
    if(access(name.c_str(), F_OK) == -1) {
        if (mkfifo(name.c_str(), mode) == -1) {
            perror("Mkfifo error");
            exit(3);
        }
    }
}