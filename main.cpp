#include <iostream>
#include "file_stream.h"
#include "producent.h"
#include "consumer.h"
#include "system_call.h"
#include <fstream>
#include <functional>
#include <utility>
#include <sstream>
#include <unistd.h>

typedef std::function<void()> ThreadCallback;

void spawn_child(const ThreadCallback &callback);
void spawnProducer(std::string, int);
void spawnConsumer(std::string, int);
void forEverySourceFileSpawnProducer(int writeDescriptor);

int main(int argc, char** argv) {
    if(argc != 2) {
        printf("Wrong number of arguments");
        exit(1);
    }
    int cosumerCount = atoi(argv[1]);
    int descriptors[2];
    pipe(descriptors);

    forEverySourceFileSpawnProducer(descriptors[1]);

    sleep(1);

    for(int i = 0; i < cosumerCount; i++) {
        std::string fileToWrite = "consumer_out/o";
        fileToWrite.append(std::to_string(i));
        spawnConsumer(fileToWrite, descriptors[0]);
    }
    return 0;
}

void spawnProducer(std::string file, int descriptor) {
    spawn_child([=]() {
        FileStream::streamFile(file, [=](char* data) {
            Producent::produce(descriptor, *data);
        });
    });
}

void spawnConsumer(std::string fileName, int descriptor) {
    spawn_child([descriptor, &fileName]() {
        FILE *fp = NULL;
        fp = fopen(fileName.c_str(), "w+");

        if (fp != NULL) {
            Consumer::consume(descriptor, [&fp](char *data) {
                fputc(*data, fp);
                fflush(fp);
            });
        } else {
            std::cout<<"Failed to open file"<<std::endl;
        }
        fclose(fp);
    });
    std::cout<<"Consumer has died";
}

void spawn_child(const ThreadCallback &callback) {
    switch (fork()) {
        case -1:
            printf("Failed to spawn...");
            exit(1);
        case 0:
            callback();
            exit(0);
    }
}

void forEverySourceFileSpawnProducer(int writeDescriptor) {
    std::string files_to_stream = SystemCall::call_command("ls producer_src");
    std::stringstream separator(files_to_stream);
    while(separator.good()){
        std::string line;
        getline(separator, line);
        spawnProducer("producer_src/"+line, writeDescriptor);
    }
}
