PROD?=5
CON?=3
PIPENAME?="somepipe"

help:
	@echo "To set number of producers user PROD env and user CON for consumer count"

run-test-unnamed:
	echo $(PROD)
	echo "Dopuszczalna różnica jednego znaku na plik źródłowy (\n na zakończenie), liczba plików źródłowych:"
	ls cmake-build-debug/producer_src | wc -l
	make cleanup
	./gen_n_test_files.sh $(PROD) 1
	make run $2
	make test

run:
	 (cd cmake-build-debug && ./unnamed_pipe $(CON))

gen_prod:
	./gen_n_test_files.sh $(PROD) 1

cleanup:
	rm cmake-build-debug/consumer_out/* || true
	rm cmake-build-debug/producer_src/* || true
test:
	echo "Producer char count"
	cat cmake-build-debug/producer_src/* | wc -c
	echo "Consumer char count"
	cat cmake-build-debug/consumer_out/* | wc -c

create-test-users:
	id -u producer || sudo useradd producer -p password
	id -u consumer || sudo useradd consumer -p password
	getent group || sudo groupadd pipy
	sudo adduser producer pipy || true
	sudo adduser consumer pipy || true

run-producer:
	echo "Pierwszy argument prod to src_plik, drugi to pipe_name"
	sudo su -l producer -c  whoami
	sudo su -l producer --preserve-environment -c  pwd && cmake-build-debug/named_producer cmake-build-debug/laba cmake-build-debug/somepipe


run-consumer:
	echo "Pierwszy argument consumer to src_plik, drugi to pipe_name"
	sudo su -l consumer -c  whoami
	sudo su -l consumer --preserve-environment -c  pwd && cmake-build-debug/named_consumer cmake-build-debug/laba-named-result cmake-build-debug/somepipe

test-named:
	(diff cmake-build-debug/laba cmake-build-debug/laba-named-result  && echo "Wynik poprawny") || echo "Wynik niepoprawny"

clean:
	rm out/*

start-named-wojtas-case:
	./spawn_producers.sh $(PROD) cmake-build-debug/laba $(PIPENAME)
	./spawn_consumers.sh $(CON) $(PIPENAME)

count-result:
	echo $(PROD)
	echo "Source file length"
	./expect_named.sh cmake-build-debug/laba $(PROD)
	echo "Outcome"
	cat out/* | wc -c
	echo "Out should be equal to src * producers"