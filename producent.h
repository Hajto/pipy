//
// Created by Haito on 10.01.2018.
//

#ifndef PROJECTS_PRODUCENT_H
#define PROJECTS_PRODUCENT_H


namespace Producent {
    void produce(int writeDescriptor, char data);
};


#endif //PROJECTS_PRODUCENT_H
